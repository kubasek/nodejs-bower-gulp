### Run forever JS script
  docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp nodemon --watch ./web index.js

### Check installed version

	docker run -it --rm kubasek/nodejs-bower-gulp node -v

	docker run -it --rm kubasek/nodejs-bower-gulp npm -v

	docker run -it --rm kubasek/nodejs-bower-gulp gulp -v

	docker run -it --rm kubasek/nodejs-bower-gulp bower -v

	docker run -it --rm kubasek/nodejs-bower-gulp git --version
	
	docker run -it --rm kubasek/nodejs-bower-gulp ruby -v
	
	docker run -it --rm kubasek/nodejs-bower-gulp gem -v
	
	docker run -it --rm kubasek/nodejs-bower-gulp sass -v

### Usage

    docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp

#### Run `node`

    docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp node

#### Run `npm`

    docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp npm

#### Run `bower`

    docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp bower

#### Run `gulp`

    docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp gulp

### npm install on windows share
	 docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp npm install --no-bin-links

### npm install on windows share without optionals	 
	 docker run -it --rm --volume=$(pwd):/data kubasek/nodejs-bower-gulp npm install --no-bin-links --no-optional